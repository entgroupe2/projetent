package com.isi.trombinoscope.Controller;

import com.isi.trombinoscope.Repositories.EnseignementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/enseignement")
@RestController
public class EnseignementController {

    @Autowired
    EnseignementRepository enseignementRepository;

   @GetMapping("/allLibele")
    public ResponseEntity<List<String>> GetAllUser(){
        List<String> enseignementList = enseignementRepository.findAllLibelles();
        System.out.println(enseignementList);
        return new ResponseEntity<>(enseignementList, HttpStatus.OK);
    }

}
