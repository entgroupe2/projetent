package com.isi.trombinoscope.Entities;

import com.isi.trombinoscope.Entities.Enum.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "contact")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeContact;

    private String nom;
    private String prenom;

//    @Enumerated(EnumType.STRING)
    private String type;
    @Column(unique = true)
    private String matricule;

//    @Enumerated(EnumType.STRING)
    private String sexe ;

    private String addreseInstitutionnelle;
    private String addresse;
    private String classe;

//    @Enumerated(EnumType.STRING)
    private String filiere;
    @Column(nullable = true)
    private Integer niveau;
//    @Enumerated(EnumType.STRING)
    private String specialite;

    private String ue;
    private String departement;
    private String poste;
    private int numeroTelephone;
    private Integer annee_academique;
}
