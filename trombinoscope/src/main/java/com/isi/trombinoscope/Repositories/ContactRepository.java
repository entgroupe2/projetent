package com.isi.trombinoscope.Repositories;

import com.isi.trombinoscope.Entities.Contact;
import com.isi.trombinoscope.Entities.UE;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository  extends JpaRepository<Contact, Long> {
    @Override
    List<Contact> findAll(Sort sort);

}
