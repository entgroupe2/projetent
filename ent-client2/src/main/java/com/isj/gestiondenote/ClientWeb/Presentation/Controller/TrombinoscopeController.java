package com.isj.gestiondenote.ClientWeb.Presentation.Controller;


import com.isj.gestiondenote.ClientWeb.utils.test.ModalWithHttpHeader;
import com.isj.gestiondenote.ClientWeb.utils.test.RequestInterceptor;
import com.isj.gestiondenote.ClientWeb.utils.test.URL;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
@Slf4j
@Controller
@CrossOrigin
public class TrombinoscopeController {
    @GetMapping("/trombinoscope")
    public ModelAndView trombinoscope(Model model, HttpSession session){
        ModalWithHttpHeader.modelTrombinoscope(model, session);
        System.out.println(model);

        System.out.println("trombinoscope");
        return new ModelAndView("pages/gestion-trombinoscope/dashboard-trombinoscope");

    }
    @GetMapping("/trombinoscope/information")
    public ModelAndView trombinoscopeInformationEtudiant(@RequestParam(required = false)String nom,
                                                         @RequestParam(required = false)String prenom,
                                                         @RequestParam(required = false)String matricule,
                                                         @RequestParam(required = false) String type,
                                                         @RequestParam(required = false) String sexe,
                                                         @RequestParam(required = false)String departement,
                                                         @RequestParam(required = false) String filiere,
                                                         @RequestParam(required = false)String niveau,
                                                         @RequestParam(required = false) String specialite,
                                                         @RequestParam(required = false) Integer annee_academique,
                                                         Model model, HttpSession session){
        ModalWithHttpHeader.modelTrombinoscope(model, session);

        HttpHeaders headers = new HttpHeaders();
        RequestInterceptor.addHeaders(headers, session);
        HttpEntity<String> httpEntity = new HttpEntity<>("", headers);

        System.out.println(model);

        System.out.println("trombinoscopeInformationEtudiant");

        ResponseEntity<Object[]> contacts = new RestTemplate().exchange(URL.BASE_URL_TRO+ "/contact/criteria?matricule="+matricule+
                "&departement="+departement+
                "&sexe="+sexe+
                "&nom="+nom+
                "&prenom="+prenom+
                "&type="+type+
                "&filiere="+filiere+
                "&niveau="+niveau+
                "&specialite="+specialite, HttpMethod.GET, httpEntity,Object[].class);


        model.addAttribute("infos", contacts.getBody());

        int nbContacts = contacts.getBody().length;

        System.out.println(nbContacts);
        
        model.addAttribute("nb", nbContacts);
        System.out.println(model);
        System.out.println("trombinoscopeInformationEtudiantTermine");


        return new ModelAndView("pages/gestion-trombinoscope/informations-étudiants");

    }
    @GetMapping("/trombinoscope/all")
    public ModelAndView trombinoscopeInformationAll(Model model, HttpSession session){
        ModalWithHttpHeader.modelTrombinoscope(model, session);

        HttpHeaders headers = new HttpHeaders();
        RequestInterceptor.addHeaders(headers, session);
        HttpEntity<String> httpEntity = new HttpEntity<>("", headers);

        System.out.println(model);
        System.out.println("trombinoscopeInformationAll");
        ResponseEntity<Object[]> contacts = new RestTemplate().exchange(URL.BASE_URL_TRO+ "contact/all", HttpMethod.GET, httpEntity,Object[].class);

        int nbContacts = contacts.getBody().length;
        System.out.println(nbContacts);
        model.addAttribute("nb", nbContacts);
        model.addAttribute("infos", contacts.getBody());




        System.out.println("yoooooo");
        return new ModelAndView("pages/gestion-trombinoscope/informations-étudiants");

    }
}
