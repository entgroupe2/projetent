package com.isi.trombinoscope.Entities;

import javax.persistence.*;

//import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "ue")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UE {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeEnseignement;

//    private String date_creation ;
//    private Date date_modification ;
    private String description ;
    private int heures_de_cours ;
    private String libelle;
    private String programme_de_cours;
    private String signature;
    private String statut_vie;
    private Long createur ;
//    private Long modificateur ;
//    private Long semestre ;
//    private Long ue ;
//    private Double credit;
}
