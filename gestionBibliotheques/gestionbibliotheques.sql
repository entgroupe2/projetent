-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 09 avr. 2024 à 15:21
-- Version du serveur : 10.4.28-MariaDB
-- Version de PHP : 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestionbibliotheques`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `creation_date`, `last_modified_date`, `code`, `description`, `nom`, `photo`) VALUES
(0, '2024-03-19 13:50:58.000000', '2024-03-26 16:30:42.000000', 'tere', 'string', 'oiu', 'string'),
(1, '2024-03-19 13:50:58.000000', NULL, 'Info101', 'ouvrage sur l\'informatique', 'Informatique', NULL),
(2, '2024-03-26 15:55:52.000000', '2024-03-26 15:55:52.000000', 'test', 'testtt', 'test', NULL),
(3, '2024-03-26 15:59:48.000000', '2024-03-26 15:59:48.000000', 'R004', 'livre avec image', 'Roman', NULL),
(4, '2024-03-26 16:06:24.000000', '2024-03-26 16:06:24.000000', 'U033', 'iudhiufwuehoiwhefw', 'unique', NULL),
(5, '2024-03-26 16:26:00.000000', '2024-03-26 16:26:00.000000', 'as', 'rest', 'aaaa', NULL),
(6, '2024-03-31 17:58:56.000000', '2024-03-31 17:58:56.000000', 'testtttt', 'testttttttt', 'testttt', NULL),
(7, '2024-04-02 16:41:36.000000', '2024-04-02 16:41:36.000000', NULL, 'c\'est un livre interessant', 'Influence et manipulation', NULL),
(8, '2024-04-02 17:51:23.000000', '2024-04-02 17:51:23.000000', 'test', 'test', 'qqqqqqqqqqqqqqqq', NULL),
(9, '2024-04-02 18:08:07.000000', '2024-04-02 18:08:07.000000', 'testtt', 'testttt', 'twst', NULL),
(10, '2024-04-02 18:12:20.000000', '2024-04-02 18:12:20.000000', 'lasted', 'lasted', 'dernier', NULL),
(11, '2024-04-02 18:25:11.000000', '2024-04-02 18:25:11.000000', 'lasted', 'lasted', 'lasted', NULL),
(12, '2024-04-02 18:39:59.000000', '2024-04-02 18:39:59.000000', 'Crijsod', 'tedaseras', 'lasted01', NULL),
(13, '2024-04-02 18:41:04.000000', '2024-04-02 18:41:04.000000', '0000001233', 'description', 'ledernier', NULL),
(19, '2024-04-06 11:35:32.000000', '2024-04-06 11:35:32.000000', '00456', 'description', 'clinton', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `dons`
--

CREATE TABLE `dons` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE `emprunt` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `date_debut` datetime(6) DEFAULT NULL,
  `date_fin` datetime(6) DEFAULT NULL,
  `date_restitution` datetime(6) DEFAULT NULL,
  `etat` bit(1) NOT NULL,
  `motif` varchar(255) DEFAULT NULL,
  `id_ouvrage` int(11) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `emprunt`
--

INSERT INTO `emprunt` (`id`, `creation_date`, `last_modified_date`, `date_debut`, `date_fin`, `date_restitution`, `etat`, `motif`, `id_ouvrage`, `id_utilisateur`) VALUES
(16, '2024-04-06 09:37:02.000000', '2024-04-06 09:37:02.000000', '2024-04-06 09:36:19.000000', '2024-04-06 09:36:19.000000', '2024-04-06 09:36:19.000000', b'1', 'apprantissage et devoir', NULL, NULL),
(17, '2024-04-06 10:47:11.000000', '2024-04-06 10:47:11.000000', '2024-04-17 00:00:00.000000', '2024-05-11 00:00:00.000000', NULL, b'0', 'iuhdosjpfwoeifpfowipfkwokepfweo', NULL, NULL),
(18, '2024-04-06 11:15:44.000000', '2024-04-06 11:15:44.000000', '2024-04-09 00:00:00.000000', '2024-05-02 00:00:00.000000', NULL, b'0', 'tt7ytuitpu 8y[8y  utp7up ut', NULL, NULL),
(21, '2024-04-06 11:41:11.000000', '2024-04-06 11:41:11.000000', '2024-04-15 00:00:00.000000', '2024-04-25 00:00:00.000000', NULL, b'0', 'description iuhwefiefpiouiu', NULL, NULL),
(22, '2024-04-06 11:42:46.000000', '2024-04-06 11:42:46.000000', '2024-04-06 00:00:00.000000', '2024-04-10 00:00:00.000000', NULL, b'0', 'description pour le motif', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE `favoris` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_ouvrage` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(23);

-- --------------------------------------------------------

--
-- Structure de la table `ligne_vente`
--

CREATE TABLE `ligne_vente` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `prix_total` double DEFAULT NULL,
  `quantite` double DEFAULT NULL,
  `id_ouvrage` int(11) DEFAULT NULL,
  `id_vente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `localisation`
--

CREATE TABLE `localisation` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nom_emplacement` varchar(255) DEFAULT NULL,
  `numero_etagere` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `localisation`
--

INSERT INTO `localisation` (`id`, `creation_date`, `last_modified_date`, `code`, `nom_emplacement`, `numero_etagere`, `section`) VALUES
(1, '2024-03-13 13:53:05.000000', NULL, 'A0013', 'section-A', '6', 'A');

-- --------------------------------------------------------

--
-- Structure de la table `ouvrage`
--

CREATE TABLE `ouvrage` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `auteur` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `disponible` bit(1) NOT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `genre` varchar(255) DEFAULT NULL,
  `nature` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `prix_unitaire` double DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `code_localisation` int(11) DEFAULT NULL,
  `isbn` varchar(255) DEFAULT NULL,
  `maison_edition` varchar(255) DEFAULT NULL,
  `date_edition` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `ouvrage`
--

INSERT INTO `ouvrage` (`id`, `creation_date`, `last_modified_date`, `auteur`, `description`, `disponible`, `fichier`, `genre`, `nature`, `nom`, `photo`, `prix_unitaire`, `type`, `id_categorie`, `code_localisation`, `isbn`, `maison_edition`, `date_edition`) VALUES
(1, '2024-03-04 13:53:59.000000', NULL, 'Sun Zu', 'livre', b'1', NULL, 'livre', 'physique', 'L\'art de la guerre', NULL, 45000, 'vieux', 1, 1, NULL, NULL, NULL),
(2, '2024-03-04 13:53:59.000000', NULL, 'George S. Clason', 'livre', b'1', NULL, 'livre', 'physique', 'homme le plus riche de babylone', NULL, 45000, 'vieux', 1, 1, NULL, NULL, NULL),
(14, '2024-04-06 09:16:16.000000', '2024-04-06 09:16:16.000000', 'Robert Cialdini', 'test', b'0', '', '', 'Physique', 'Influence et manipulation', NULL, 0, NULL, NULL, NULL, NULL, 'Auchamp', NULL),
(15, '2024-04-06 09:21:57.000000', '2024-04-06 09:21:57.000000', 'test', 'test', b'0', '', 'fin', 'Physique', 'test', NULL, 0, NULL, NULL, NULL, NULL, 'test', NULL),
(20, '2024-04-06 11:37:51.000000', '2024-04-06 11:37:51.000000', 'clinotn', 'tetstttt', b'0', '', 'ewtwetwet', 'Physique', 'titre', NULL, 0, NULL, NULL, NULL, NULL, 'Auchamp', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `pass_word` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `vente`
--

CREATE TABLE `vente` (
  `id` int(11) NOT NULL,
  `creation_date` datetime(6) NOT NULL,
  `last_modified_date` datetime(6) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `dons`
--
ALTER TABLE `dons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKjppiui1aatfk5avgjbke8so3i` (`id_utilisateur`);

--
-- Index pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1kq372qf3mq8d33ab9n2p0qix` (`id_ouvrage`),
  ADD KEY `FK7150jwvtptangtdg5h4yaa6ts` (`id_utilisateur`);

--
-- Index pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK9dth1rx8kkygt4ssfrytqsdd8` (`id_ouvrage`);

--
-- Index pour la table `ligne_vente`
--
ALTER TABLE `ligne_vente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdy66r7kyobvv53k5wtue3a9mc` (`id_ouvrage`),
  ADD KEY `FKljcsivgt086wr14862iu9uxiw` (`id_vente`);

--
-- Index pour la table `localisation`
--
ALTER TABLE `localisation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ouvrage`
--
ALTER TABLE `ouvrage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5or0e98dkh2al4yrk6lxqi47r` (`id_categorie`),
  ADD KEY `FKag3clas079ufela899ebav6k6` (`code_localisation`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKk28wuteju9orwm998sna1j28e` (`id_role`);

--
-- Index pour la table `vente`
--
ALTER TABLE `vente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKr2dc9u0rdqi3h31yk8ang0h6i` (`id_utilisateur`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `dons`
--
ALTER TABLE `dons`
  ADD CONSTRAINT `FKjppiui1aatfk5avgjbke8so3i` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `FK1kq372qf3mq8d33ab9n2p0qix` FOREIGN KEY (`id_ouvrage`) REFERENCES `ouvrage` (`id`),
  ADD CONSTRAINT `FK7150jwvtptangtdg5h4yaa6ts` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `favoris`
--
ALTER TABLE `favoris`
  ADD CONSTRAINT `FK9dth1rx8kkygt4ssfrytqsdd8` FOREIGN KEY (`id_ouvrage`) REFERENCES `ouvrage` (`id`);

--
-- Contraintes pour la table `ligne_vente`
--
ALTER TABLE `ligne_vente`
  ADD CONSTRAINT `FKdy66r7kyobvv53k5wtue3a9mc` FOREIGN KEY (`id_ouvrage`) REFERENCES `ouvrage` (`id`),
  ADD CONSTRAINT `FKljcsivgt086wr14862iu9uxiw` FOREIGN KEY (`id_vente`) REFERENCES `vente` (`id`);

--
-- Contraintes pour la table `ouvrage`
--
ALTER TABLE `ouvrage`
  ADD CONSTRAINT `FK5or0e98dkh2al4yrk6lxqi47r` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FKag3clas079ufela899ebav6k6` FOREIGN KEY (`code_localisation`) REFERENCES `localisation` (`id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FKk28wuteju9orwm998sna1j28e` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`);

--
-- Contraintes pour la table `vente`
--
ALTER TABLE `vente`
  ADD CONSTRAINT `FKr2dc9u0rdqi3h31yk8ang0h6i` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
