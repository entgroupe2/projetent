package com.isj.gestiondenote.ClientWeb.Presentation.Controller;

import com.isj.gestiondenote.ClientWeb.Model.dto.UserDto;
import com.isj.gestiondenote.ClientWeb.Model.dto.*;
import com.isj.gestiondenote.ClientWeb.Model.entities.*;
import com.isj.gestiondenote.ClientWeb.Model.modeletat.*;
import com.isj.gestiondenote.ClientWeb.utils.test.*;
import com.isj.gestiondenote.ClientWeb.utils.test.ModalWithHttpHeader;
import com.isj.gestiondenote.ClientWeb.utils.test.URL;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Slf4j
@Controller
public class UserController {
    @RequestMapping(value = {"/"})
    public ModelAndView pageConnexionForm(Model model){
        final ModelAndView modelAndView = new ModelAndView();
        model.addAttribute("user", new UserDto());
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping("/connexion")
    public String pageConnexion(@ModelAttribute(name = "utilisateur") Utilisateur utilisateur, Model model){
        return "redirect:dashboard";
    }

    @GetMapping("/deconnexion")
    public String deconnexion(Model model){
        return "redirect:/";
    }

    @GetMapping("/dashboard")
    public String pageAccueil(Model model){
  //      Modal.model(model);

        return "Accueil";
    }

    @PostMapping("/login")
    public String login(Model model, @ModelAttribute UserDto userDto , HttpSession session) throws URISyntaxException {
        System.out.println("yes");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UserDto> httpEntity = new HttpEntity<>(userDto, headers);
        RestTemplate restTemplate = new RestTemplate();
        UserResponseDto userResponseDto= new UserResponseDto();
        userResponseDto = restTemplate.postForObject(new URI(URL.BASE_URL_AUTH+"/user/login"),httpEntity, UserResponseDto.class);
        System.out.println("Utilisateur:"+userResponseDto.toString());
        session.setAttribute("email",userResponseDto.getUsername());
        session.setAttribute("matricule",userResponseDto.getMatricule());
        session.setAttribute("filiere",userResponseDto.getFiliere());
        session.setAttribute("classe",userResponseDto.getClasse());
        session.setAttribute("name",userResponseDto.getName());
        session.setAttribute("accessToken",userResponseDto.getAccessToken());
        session.setAttribute("authorithies",userResponseDto.getAuthorities());
        List<AuthorityDto> authorities = userResponseDto.getAuthorities();
        session.setAttribute("authorithy", authorities.get(0).getAuthority());
        System.out.println(userResponseDto.getAccessToken());
        return "redirect:dashboard";
    }

    @GetMapping("/logout")
        public String disconnectUser(Model model,HttpSession session){
            session.invalidate();
            return "redirect:/";
        }


    @GetMapping("/gestionnote")
    public String pageProfile(Model model, HttpSession session){
        ModalWithHttpHeader.model(model, session);
        return "layout/gestionNote/gestNote";
    }

    @GetMapping("/profil")
    public String pageProfileEtudiant(Model model,HttpSession session){
        ModalWithHttpHeader.model(model, session);
        return "layout/gestionNote/monProfil";
    }

    @GetMapping("/parcours")
    public String PageParcoursEtudiant(Model model,HttpSession session){
        ModalWithHttpHeader.model(model, session);
        return "layout/gestionNote/academique";
    }

    @GetMapping("/gestionintervention")
    public ModelAndView pageintervention(Model model, HttpSession session){
        ModalWithHttpHeader.model(model, session);
        String accessToken = (String) session.getAttribute("accessToken");
        String matricule = (String) session.getAttribute("matricule");
//        RestTemplate restTemplate = new RestTemplate();
//        Integer nbretotal = restTemplate.getForObject(URL.BASE_URL_INT + "nbreTotalInt", Integer.class);
//        if (nbretotal!=null){
//            model.addAttribute("nbreTotal", nbretotal);
//        }
        model.addAttribute("accessToken", accessToken);
        model.addAttribute("matricule", matricule);
        System.out.println(model);

        RestTemplate restTemplate = new RestTemplate();
        System.out.println("nbreeeee");
        Long nombreInterventions= restTemplate.getForObject(URL.BASE_URL_INT+ "nombre/intervention/totalEtud/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsEtud", nombreInterventions);

        Long nombreInterventionsPerso= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/Departement/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsPerso", nombreInterventionsPerso);

        Long nombreInterventionsPersoTraite= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/totalPersoTraite/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsPersoTraite", nombreInterventionsPersoTraite);

        Long nombreInterventionsPersoNontraite= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/totalPersoNontraite/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsPersoNontraite", nombreInterventionsPersoNontraite);

        Long nombreInterventionsPersoEncours= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/totalPersoEncours/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsPersoEncours", nombreInterventionsPersoEncours);


        Long nombreInterventionsEtudTraite= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/totalEtudTraite/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsEtudTraite", nombreInterventionsEtudTraite);

        Long nombreInterventionsEtudEncours= restTemplate.getForObject(URL.BASE_URL_INT+ "nombre/intervention/totalEtudEncours/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsEtudEncours", nombreInterventionsEtudEncours);

        Long nombreInterventionsEtudNontraite= restTemplate.getForObject(URL.BASE_URL_INT+ "/nombre/intervention/totalEtudNontraite/" + matricule, Long.class);
        //System.out.println(nombreInterventions);
        model.addAttribute("nombreInterventionsEtudNontraite", nombreInterventionsEtudNontraite);


        //Long nombreInterventionsD=re
        return new ModelAndView("pages/gestion-interventions/dashboard-interventions");

    }
//    @GetMapping("/trombinoscope")
//    public ModelAndView trombinoscope(Model model, HttpSession session){
//        ModalWithHttpHeader.model(model, session);
//        String accessToken = (String) session.getAttribute("accessToken");
//        String matricule = (String) session.getAttribute("matricule");
//        model.addAttribute("accessToken", accessToken);
//        model.addAttribute("matricule", matricule);
//        System.out.println(model);
//        return new ModelAndView("pages/gestion-trombinoscope/dashboard-trombinoscope");
//
//    }
//    @GetMapping("/trombinoscope/information/etudiant")
//    public ModelAndView trombinoscopeInformationEtudiant(Model model, HttpSession session){
//        ModalWithHttpHeader.model(model, session);
//        String accessToken = (String) session.getAttribute("accessToken");
//        String matricule = (String) session.getAttribute("matricule");
//        model.addAttribute("accessToken", accessToken);
//        model.addAttribute("matricule", matricule);
//
//
//        System.out.println("yooooooooooooooooooooooooooo");
//        RestTemplate restTemplate = new RestTemplate();
//        Object[] ing1 = restTemplate.getForObject(URL.BASE_URL_TRO + "Trombi/EtudING1", Object[].class);
//        model.addAttribute("ing1", ing1);
//
//        Object[] ing4 = restTemplate.getForObject(URL.BASE_URL_TRO + "Trombi/EtudING4", Object[].class);
//        model.addAttribute("ing4", ing4);
//
//        Object[] lic1 = restTemplate.getForObject(URL.BASE_URL_TRO + "Trombi/EtudLIC1", Object[].class);
//        model.addAttribute("lic1", lic1);
//
////        System.out.println(infos);
//        System.out.println(model);
//        System.out.println("yooooooooooooooooooooooooooo");
//
//
//        return new ModelAndView("pages/gestion-trombinoscope/informations-étudiants");
//
//    }
//    @GetMapping("/trombinoscope/information/personnel")
//    public ModelAndView trombinoscopeInformationPersonnel(Model model, HttpSession session){
//        ModalWithHttpHeader.model(model, session);
//        String accessToken = (String) session.getAttribute("accessToken");
//        String matricule = (String) session.getAttribute("matricule");
//        model.addAttribute("accessToken", accessToken);
//        model.addAttribute("matricule", matricule);
//
//        RestTemplate restTemplate = new RestTemplate();
//        Object[] persoIT = restTemplate.getForObject(URL.BASE_URL_TRO + "Trombi/PersoIT", Object[].class);
//        model.addAttribute("persoIT", persoIT);
//
//        System.out.println(model);
//        return new ModelAndView("pages/gestion-trombinoscope/informations-personnel");
//
//    }
//
//    @GetMapping("/trombinoscope/information/enseignant")
//    public ModelAndView trombinoscopeInformationEnseignant(Model model, HttpSession session){
//        ModalWithHttpHeader.model(model, session);
//        String accessToken = (String) session.getAttribute("accessToken");
//        String matricule = (String) session.getAttribute("matricule");
//        model.addAttribute("accessToken", accessToken);
//        model.addAttribute("matricule", matricule);
//
//        RestTemplate restTemplate = new RestTemplate();
//        Object[] teachP = restTemplate.getForObject(URL.BASE_URL_TRO + "Trombi/Pedagogie", Object[].class);
//        model.addAttribute("teachP", teachP);
//
//        System.out.println(model);
//        return new ModelAndView("pages/gestion-trombinoscope/informations-enseignant");
//
//    }
///template/pages/gestion-trombinoscope/informations-étudiants.html
///template/pages/gestion-trombinoscope/informations-personnel.html

//    @GetMapping("/listeintervention")
//    public ModelAndView listeintervention(Model model, HttpSession session){
//        ModalWithHttpHeader.model(model, session);
//        String accessToken = (String) session.getAttribute("accessToken");
//        model.addAttribute("accessToken", accessToken);
//        System.out.println(model);
//        return new ModelAndView("pages/gestion-interventions/liste-des-interventions");
//
//    }

}
