CREATE DATABASE  IF NOT EXISTS `bdtrombinoscope` ;
USE `bdtrombinoscope`;
-- MySQL dump 10.13  Distrib 8.0.32, for Win64 (x86_64)
--
-- Host: localhost    Database: bdtrombinoscope
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `code_contact` bigint NOT NULL AUTO_INCREMENT,
  `addrese_institutionnelle` varchar(255) DEFAULT NULL,
  `addresse` varchar(255) DEFAULT NULL,
  `classe` varchar(255) DEFAULT NULL,
  `departement` varchar(255) DEFAULT NULL,
  `filiere` int DEFAULT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `niveau` int DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `numero_telephone` int NOT NULL,
  `poste` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `sexe` varchar(255) DEFAULT NULL,
  `specialite` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_contact`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'quentin.tiyo@institutsaintjean.org',NULL,NULL,NULL,0,'2021i081',4,'Tiyo',234546324,NULL,'Quentin','MASCULIN','SRT','Etudiant',NULL),(2,'jerry.wafo@institutsaintjean.org',NULL,NULL,NULL,0,'2021i074',4,'Wafo',694993264,NULL,'jerry','MASCULIN','ISI','Etudiant',NULL),(3,'keisper.tiyouh@institutsaintjean.org',NULL,NULL,'IT',NULL,'2021p134',NULL,'Tiyouh',484357343,'Chef des Supports','Keisper','MASCULIN',NULL,'Personnel',NULL),(4,'arthur.pessa@institutsaintjean.org',NULL,NULL,'Pedagogie',NULL,'2021e154',NULL,'Pessa',342098543,NULL,'Arthur','MASCULIN',NULL,'Enseignant','Architecture Web, Developpement Mobile'),(5,'irene.atangana ntsama@institutsaintjean.org',NULL,NULL,'Marketing',NULL,'2021p155',NULL,'Atangana Ntsama',329835434,'Responsable','Irene','FEMININ',NULL,'Personnel',NULL);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ue`
--

DROP TABLE IF EXISTS `ue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ue` (
  `code_enseignement` bigint NOT NULL AUTO_INCREMENT,
  `createur` bigint DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `heures_de_cours` int NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `programme_de_cours` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `statut_vie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_enseignement`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ue`
--

LOCK TABLES `ue` WRITE;
/*!40000 ALTER TABLE `ue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ue` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-03-07 21:43:28
