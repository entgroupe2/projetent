package com.isi.trombinoscope.Repositories;

import com.isi.trombinoscope.Entities.UE;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnseignementRepository extends JpaRepository<UE, Long> , JpaSpecificationExecutor<UE>  {
//    List<Enseignement> findAllByLibelle();
    @Query("SELECT e.libelle FROM UE e ORDER BY e.libelle ASC")
    List<String> findAllLibelles();
}