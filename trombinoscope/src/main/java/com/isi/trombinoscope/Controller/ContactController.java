package com.isi.trombinoscope.Controller;

import com.isi.trombinoscope.Entities.Contact;
import com.isi.trombinoscope.Entities.Enum.Filiere;
import com.isi.trombinoscope.Entities.Enum.Sexe;
import com.isi.trombinoscope.Entities.Enum.Specialite;
import com.isi.trombinoscope.Entities.Enum.Type;
import com.isi.trombinoscope.Repositories.ContactRepository;
import com.isi.trombinoscope.Service.ContactService;
import com.thoughtworks.xstream.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RequestMapping("/contact")
@RestController
@CrossOrigin
public class ContactController {
    @Autowired
    ContactRepository contactRepository;
    @Autowired
    ContactService contactService;

    @GetMapping("/all")
    public ResponseEntity<List<Contact>> getAllContact(){
        Sort sort = Sort.by(Sort.Order.asc("type"), Sort.Order.asc("filiere"), Sort.Order.asc("classe"), Sort.Order.asc("nom"));
        List<Contact> contactList = contactRepository.findAll(sort);
        List<String> EtudList = null;
        List<String> TeachList = null;
        List<String> PersoList = null;

        for (Contact contact : contactList){
            if (contact.getType() == "Etudiant"){
                System.out.println("j'entre ici");
            EtudList.add(contact.getType());}
//            if (contact.getType() == "Enseignant"){
//                TeachList.add(contact.getType());}
//            if (contact.getType() == "Personnel"){
//                PersoList.add(contact.getType());}
        }
        System.out.println(EtudList);
        System.out.println(TeachList);
        System.out.println(PersoList);
        System.out.println(contactList);
        return new ResponseEntity<>(contactList, HttpStatus.OK);
    }
    @GetMapping("/{codeContact}")
    public ResponseEntity<Contact> getContact(@PathVariable("codeContact") Long codeContact){
        Contact contact = contactRepository.findById(codeContact).get();
        System.out.println(contact);
        return new ResponseEntity<>(contact, HttpStatus.OK);
    }
//    @PostMapping("/save")
//    public ResponseEntity<Contact> creerContact(@RequestBody Contact newContact){
//        Contact contact = contactService.creerContact(newContact);
//        System.out.println(contact);
//        return new ResponseEntity<>(contact, HttpStatus.OK);
//    }
    @PostMapping("/save")
    public ResponseEntity<List<Contact>> creerContact(@RequestBody Contact newContact){
        Contact contact = contactService.creerContact(newContact);
        System.out.println(contact);

        return getAllContact();
    }
    @PostMapping("/save/List")
    public ResponseEntity<List<Contact>> creerManyContact(@RequestBody List<Contact> newContact){
        for (Contact contact:newContact) {
            Contact contact1 = contactService.creerContact(contact);
            System.out.println(contact1);
        }
        return getAllContact();
    }
    @GetMapping("/criteria")
    public ResponseEntity<List<Contact>> getCriteria(@RequestParam(required = false)String nom,
                                                     @RequestParam(required = false)String prenom,
                                                     @RequestParam(required = false)String matricule,
                                                     @RequestParam(required = false)String type,
                                                     @RequestParam(required = false)String sexe,
                                                     @RequestParam(required = false)String departement,
                                                     @RequestParam(required = false)String filiere,
                                                     @RequestParam(required = false)Integer niveau,
                                                     @RequestParam(required = false) String specialite,
                                                     @RequestParam(required = false) Integer annee_academique){
        List<Contact> results = contactService.findContactsByCriteria(nom,prenom, matricule, type, sexe,departement,filiere,niveau,specialite, annee_academique);
        System.out.println(results);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }


    @GetMapping("annees_academiques")
    @ResponseBody
    public ResponseEntity<Set<Integer>> getAnneesAcademiques(){
        Set<Integer> List = contactService.listOfContacts();
        return new ResponseEntity<>(List, HttpStatus.OK);
        }

}
