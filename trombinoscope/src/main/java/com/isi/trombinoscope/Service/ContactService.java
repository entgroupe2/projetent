package com.isi.trombinoscope.Service;

import com.isi.trombinoscope.Entities.Contact;

import java.util.List;
import java.util.Set;

public interface ContactService {
    public Contact creerContact(Contact newContact);

    public Set<Integer> listOfContacts();
    public List<Contact> findContactsByCriteria(String nom, String prenom, String matricule, String type, String sexe, String departement, String filiere, Integer niveau, String specialite, Integer annee_academique);
}
