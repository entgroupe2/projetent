package com.isi.trombinoscope.Service.Implementation;

import com.isi.trombinoscope.Entities.Contact;
import com.isi.trombinoscope.Repositories.ContactRepository;
import com.isi.trombinoscope.Service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ContactServiceImpl implements ContactService {
    @Autowired
    private ContactRepository contactRepository;
    private Contact contact;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Contact creerContact(Contact newContact) {
        if(newContact.getAddreseInstitutionnelle()==null){
            newContact.setAddreseInstitutionnelle(newContact.getPrenom().toLowerCase()+"."+newContact.getNom().toLowerCase()+"@institutsaintjean.org");
        }
        contactRepository.save(newContact);
        return newContact;
    }

    @Override
    public Set<Integer> listOfContacts() {

    List<Contact> allContacts = contactRepository.findAll();

        Set<Integer> anneesAcademiques = new HashSet<>();

        for (Contact contact : allContacts) {
            anneesAcademiques.add(contact.getAnnee_academique());
        }
        return anneesAcademiques;
    }

    public List<Contact> searchContacts(String nom,String prenom, String matricule, String type, String sexe, String departement, String filiere, Integer niveau,String specialite,Integer annee_academique, EntityManager entityManager) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);

        Root<Contact> contactRoot = cq.from(Contact.class);
        List<Predicate> predicates = new ArrayList<>();

        if (nom != null && nom!="") {
//            predicates.add(cb.equal(contactRoot.get("nom"), nom));
            predicates.add(cb.like(contactRoot.get("nom"),"%"+nom+"%"));

        }
        if (prenom != null && prenom!="") {
//            predicates.add(cb.equal(contactRoot.get("nom"), nom));
            predicates.add(cb.like(contactRoot.get("prenom"),"%"+prenom+"%"));

        }
        if (departement != null && departement!="") {
            predicates.add(cb.equal(contactRoot.get("departement"), departement));
        }
        if (filiere != null && filiere!="") {
            predicates.add(cb.equal(contactRoot.get("filiere"), filiere));
        }
        if (type != null && type!="") {
            predicates.add(cb.equal(contactRoot.get("type"), type));
        }
        if (matricule != null && matricule!="") {
            predicates.add(cb.equal(contactRoot.get("matricule"), matricule));
        }
        if (sexe != null && sexe!="") {
            predicates.add(cb.equal(contactRoot.get("sexe"), sexe));
        }
        if (niveau != null && niveau!= 0) {
            predicates.add(cb.equal(contactRoot.get("niveau"), niveau));
        }
        if (specialite != null && specialite!="") {
            predicates.add(cb.equal(contactRoot.get("specialite"), specialite));
        }
        if ((annee_academique != null) && (annee_academique !=0 )) {
            predicates.add(cb.equal(contactRoot.get("annee_academique"), annee_academique));
        }

        cq.where(cb.and(predicates.toArray(new Predicate[0])));
        TypedQuery<Contact> query = entityManager.createQuery(cq);
        System.out.println(query);
        System.out.println(query.getResultList());
        return query.getResultList();
    }

    public List<Contact> findContactsByCriteria(String nom, String prenom, String matricule, String type, String sexe, String departement, String filiere, Integer niveau, String specialite, Integer annee_academique) {
        return searchContacts(nom,prenom, matricule, type, sexe, departement, filiere, niveau,specialite,annee_academique, entityManager);
    }
}
